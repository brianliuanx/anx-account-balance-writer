'use strict';

let log4js = require('log4js');
let loggerConf = require('../conf/logger-conf.json');

/**
 * common initialization logic for all mocha tests.
 */
function init(){

  //ignore SSL cert error
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

  //configure logger
  log4js.configure(loggerConf);
  // let logger = log4js.getLogger();
  // logger.debug('logger hello ... 1.');
  // logger.info('logger hello ... 2.');
  // logger.warn('logger hello ... 3.');
  // logger.error('logger hello ... 4.');
  // logger.fatal('logger hello ... 5.');

}

module.exports = {
  init: init
};
