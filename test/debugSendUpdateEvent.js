'use strict';

let BigNumber = require('bignumber.js');
let log4js = require('log4js');
let path = require('path');
let kafka = require('kafka-node');

let config = require('../conf/balanceWriter-conf.json');
let setupInit = require('../common/setup').init;

let logger = log4js.getLogger(path.basename(__filename));
setupInit();

let kafkaClient = new kafka.KafkaClient({ 
  kafkaHost: config.kafka.host,
  clientId: config.kafka.clientId
});

let producer = new kafka.Producer(kafkaClient, {
  requireAcks: 1,
  ackTimeoutMs: 100,
  partitionerType: 0
});

producer.on('ready', ()=>{
  logger.debug('kafka producer: ready');

  let messagePayload = {
    partition: config.topic.balanceChangeEvent.partition,
    topic: config.topic.balanceChangeEvent.name,
    messages: 
      `DESCRIPTION.2=test1, CLASS=anxgroup.ledger.api.protocol.v1.JournalEntriesCreated, 
       FLOW_NAME=flowName, TIMESTAMP.2=111, TIMESTAMP.1=111, JOURNAL_ENTRY_ID.2=2, 
       CREDIT_ACCOUNT_NEW_BAL.1=1, AMOUNT.2=10, 
       CREDIT_ACCOUNT_NEW_BAL.2=50, CREDIT_ACCOUNT_CLASS.2=TRADING, 
       DEBIT_ACCOUNT_UUID.1=f4948f50-9ce9-4884-a5a2-7d9021bf3a8e, 
       DEBIT_ACCOUNT_UUID.2=82edd47e-dd34-493a-b547-0e118168b2b8, 
       CREDIT_ACCOUNT_CLASS.1=TRADING, 
       DEBIT_ACCOUNT_NEW_BAL.1=0, DEBIT_BALANCE_TYPE.1=AVAILABLE, 
       DEBIT_ACCOUNT_NEW_BAL.2=34, DEBIT_BALANCE_TYPE.2=AVAILABLE, 
       AMOUNT.1=1, TIMESTAMP=111, JOURNAL_ENTRY_ID.1=1, 
       FLOW_REF_ID=flowRefId, 
       CREDIT_ACCOUNT_UUID.1=53e7038d-877c-40a5-8f93-d81d3e438fdd, 
       CREDIT_ACCOUNT_UUID.2=5e5840ad-7bd7-4056-8503-d38e787a4368, 
       CURRENCY.1=HKD, CREDIT_BALANCE_TYPE.2=SUSPENSE, 
       CURRENCY.2=BTC, CREDIT_BALANCE_TYPE.1=SUSPENSE, 
       SRC_SEQ_NO=4, DEBIT_ACCOUNT_CLASS.2=TRADING, 
       DEBIT_ACCOUNT_CLASS.1=TRADING, 
       DEBIT_USER.2=sameUser, 
       DEBIT_USER.1=debitUser, REPLAY=false, CREDIT_USER.1=creditUser, 
       CREDIT_USER.2=sameUser, 
       EVENT_ID=10, DEBIT_ACCOUNT_OLD_BAL.2=44, DESCRIPTION.1=test1, 
       DEBIT_ACCOUNT_OLD_BAL.1=1, 
       CREDIT_ACCOUNT_OLD_BAL.2=40, CREDIT_ACCOUNT_OLD_BAL.1=0`
       .replace(/\s*\n\s*/g,' '),
  };
  producer.send([ messagePayload ], (err, data)=>{
    if (err) {
      logger.error('error in kafka producer callback: %j', err);
      return;
    }
    logger.debug('data in kafka producer callback: %j', data);
  });

  setTimeout(()=>{
    logger.debug('time up, close it.');
    kafkaClient.close(()=>{
      logger.debug('kafka client: closed');
    });
  }, 1000);
  
});

producer.on('error', (err)=>{
  logger.error('error in kafka producer: %j', err);
});
