'use strict';

class AccountBalanceChanged {

  /**
   * 
   * @param {string} accountUuid 
   * @param {string} balanceType 
   * @param {BigNumber} amount 
   */
  constructor(accountUuid, balanceType, amount) {
  }

}

module.exports = {
  AccountBalanceChanged: AccountBalanceChanged
};
