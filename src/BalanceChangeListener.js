'use strict';

let BigNumber = require('bignumber.js');
let log4js = require('log4js');
let path = require('path');
let kafka = require('kafka-node');

let AccountBalanceChanged = require('./AccountBalanceChanged').AccountBalanceChanged;
let JournalEntriesCreated = require('./JournalEntriesCreated').JournalEntriesCreated;
let dbRepo = require('./DbRepo').dbRepo;

let config = require('../conf/balanceWriter-conf.json');
let setupInit = require('../common/setup').init;

let logger = log4js.getLogger(path.basename(__filename));
setupInit();

class BalanceChangeListener {

  constructor(config){
    this.kafkaClient = new kafka.KafkaClient({ 
      kafkaHost: config.kafka.host,
      clientId: config.kafka.clientId
    });
    this.consumer = new kafka.Consumer(this.kafkaClient, [ 
      {
        partition: config.topic.balanceChangeEvent.partition,
        topic: config.topic.balanceChangeEvent.name,
      },
    ], { autoCommit: true });
    
    this.kafkaClient.on('connect',()=>{
      logger.info('kafkaClient: connected.');
    });
    this.kafkaClient.on('close', ()=>{
      logger.info('kafkaClient: closed.');
    });

    this.consumer.on('message', (message)=>{
      logger.debug('onMessage: %j', message);
      let journalEntriesCreated = new JournalEntriesCreated(message.value);
      this._printDebugLogForParseMessage(journalEntriesCreated);

      // marshal data and notify for balance change.
      let debitAccountsMap = journalEntriesCreated.listEntries.get('DEBIT_ACCOUNT_UUID');
      let creditAccountsMap = journalEntriesCreated.listEntries.get('CREDIT_ACCOUNT_UUID');
      let debitAmountsMap = journalEntriesCreated.listEntries.get('DEBIT_ACCOUNT_NEW_BAL');
      let creditAmountsMap = journalEntriesCreated.listEntries.get('CREDIT_ACCOUNT_NEW_BAL');
      let debitBalanceTypesMap = journalEntriesCreated.listEntries.get('DEBIT_BALANCE_TYPE');
      let creditBalanceTypesMap = journalEntriesCreated.listEntries.get('CREDIT_BALANCE_TYPE');
      for (let i=1;i<=Object.keys(debitAccountsMap).length;i++) {
        this.onBalanceChangeEvent({
          accountUuid: debitAccountsMap[i],
          balanceType: debitBalanceTypesMap[i],
          amount: debitAmountsMap[i],
        });

        this.onBalanceChangeEvent({
          accountUuid: creditAccountsMap[i],
          balanceType: creditBalanceTypesMap[i],
          amount: creditAmountsMap[i],
        });
      }
    });
  }

  close(){
    this.consumer.close(false);
    this.kafkaClient.close();
  }

  /**
   * 
   * @param {AccountBalanceChanged} balanceChangeEvent 
   */
  onBalanceChangeEvent(event){
    logger.debug(`balanceChangeEvent:${event.accountUuid}<${event.balanceType}>=${event.amount}`);
    
    dbRepo.findAccountByUuid(event.accountUuid).then(data=>{
      logger.debug(`onBalanceChangeEvent account[${event.accountUuid}] loaded: `, data);
      if (!data || data.length == 0) {
        throw new Error(`account not found:${event.accountUuid}.`);
      }
      let account = data[0];
      // account = { id: 10,
      //   version: 0,
      //   balance: 0,
      //   ccy: 'CAD',
      //   owner_id: 1002,
      //   hold: 0,
      //   uuid: '39c16aec-1601-44e8-9579-5e3249f3bb03' }
      return dbRepo.updateAccountBalance(account, event.amount);

    }).then(result=>{
      logger.debug(`onBalanceChangeEvent:account=[${event.accountUuid}] result=%j`, result);

    }).catch(err=>{
      logger.error('onBalanceChangeEvent:err=%j', err);
    });

  }

  _printDebugLogForParseMessage(journalEntriesCreated){
    // debug printing for detail of theJournalEntriesCreated
    if (journalEntriesCreated.singleEntries.size > 0) {
      for (let k of journalEntriesCreated.singleEntries.keys()) {
        let v = journalEntriesCreated.singleEntries.get(k);
        logger.debug(`message:journalEntriesCreated: key/value = ${k}:${v}`);
      }
    }
    if (journalEntriesCreated.listEntries.size > 0) {
      for (let k of journalEntriesCreated.listEntries.keys()) {
        let v = journalEntriesCreated.listEntries.get(k);
        logger.debug(`message:journalEntriesCreated: key/list = ${k}:${JSON.stringify(v)}`);
      }
    }
  }

}

//////////////////////////////////////////////
let listener = new BalanceChangeListener(config);

// setTimeout(()=>{
//   logger.debug('time up, close it.');
//   listener.close();
// }, 50000);
