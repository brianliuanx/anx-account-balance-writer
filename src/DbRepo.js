let jugglingdb = require('jugglingdb');
let BigNumber = require('bignumber.js');
let log4js = require('log4js');
let path = require('path');
let Promise = require('bluebird');

let config = require('../conf/balanceWriter-conf.json');
let setupInit = require('../common/setup').init;

let logger = log4js.getLogger(path.basename(__filename));
setupInit();

let Schema = jugglingdb.Schema;

class DbRepo {

  constructor(){
    //define data source
    this.schema = new Schema('mysql', config.database);
    
    //define orm model
    this.models = {};
    this.models.Account = this.schema.define('Account', {
      id: Number,
      uuid: String,
      version: Number,
      owner_id: Number,
      ccy: String,
      balance: String,
      hold: String,
    }, {
      table: 'account'
    });

  }

  /**
   * 
   * @param {string} accountUuid 
   * @returns {Promise<Account>}
   */
  async findAccountByUuid(accountUuid){
    return new Promise((resolve, reject)=>{
      this.models.Account.all({
        where: {
          uuid: accountUuid
        }
      }).then((data)=>{
        logger.debug('findAccountByUuid:data=', data);
        
        resolve(data);
      }).catch(err=>{
        logger.error('findAccountByUuid:err=', err);
        reject(err);
      });
    });
  }

  /**
   * 
   * @param {Account} account 
   * @param {BigNumber} balance 
   * @returns {Promise<UpdateResultDto>}
   */
  async updateAccountBalance(account, balance){
    return new Promise((resolve, reject)=>{
      this.models.Account.update({
        update: {
          version: (account.version) + 1,
          balance: balance
        }, where: { 
          id: account.id,
          version: account.version
        }
      }).then((updateResultDtoList)=>{
        let updateResultDto = updateResultDtoList[0];
        logger.debug('updateAccountBalance:updateResultDto=%j', updateResultDto);
        
        if (updateResultDto.affectedRows > 0) {
          resolve(updateResultDto);
        } else {
          let err = `update without affected row: possible missed the target[${account.id}].`;
          logger.error('updateAccountBalance:err=%j', err);
          reject(new Error(err));
        }
      }).catch(err=>{
        logger.error('updateAccountBalance:err=%j', err);
        reject(err);
      });

    });
  }

}

class UpdateResultDto {

  /**
   * 
   * @param {number} fieldCount 
   * @param {number} affectedRows 
   * @param {number} changedRows 
   */
  constructor(fieldCount, affectedRows, changedRows){
    this.fieldCount = fieldCount;
    this.affectedRows = affectedRows;
    this.changedRows = changedRows;
  }

}

let dbRepo = new DbRepo();

module.exports = {
  dbRepo: dbRepo
};
