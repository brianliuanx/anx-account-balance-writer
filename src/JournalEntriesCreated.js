'use strict';

class JournalEntriesCreated {

  constructor(rawMessage){
    /**
     * key = key without index suffix, 
     * value = data (not array).
     */
    this.singleEntries = new Map();
    
    /**
     * key = key without index suffix, 
     * value = Object of entries (key=index, value=data).
     */
    this.listEntries = new Map();

    for (let pattern of [
      /([A-Z\_]+)(=)([^,]*)(\, )/g, // FLOW_NAME=flowName,
      /([A-Z\_]+)(=)([^,]*)$/g, // FLOW_NAME=flowName$
    ]) {
      let matched;
      while (matched = pattern.exec(rawMessage)) {
        let key = matched[1];
        let value = matched[3];
        this.singleEntries.set(key, matched[3]);
      }
    }

    for (let pattern of [
      /([A-Z\_]+)\.([0-9])+(=)([^,]*)(\, )/g, // DESCRIPTION.2=test1, 
      /([A-Z\_]+)\.([0-9])+(=)([^,]*)$/g,     // DESCRIPTION.2=test1$
    ]) {
      let matched;
      while (matched = pattern.exec(rawMessage)) {
        let key = matched[1];
        let org = this.listEntries.get(key);
        let index = +matched[2];
        let value = matched[4];
        if (org != null) {
          org[index] = value;
        } else {
          let newObj = {};
          newObj[index] = value;
          this.listEntries.set(key, newObj);
        }
      }
    }
  }

}

module.exports = {
  JournalEntriesCreated
};
