# balanceWriter
---------------

## Runtime

- nodeJS 8.9

## Configuration

- main config: `/conf/balanceWriter-conf.json`
- logger config: `/conf/logger-conf.json`

## Run

```
node src/balanceChangeListener.js
```

## Debug

To send update event message directly 
(so that it will be picked up by our listener)
```
node test/debugSendUpdateEvent.js
```
